rm(list = ls())

library(png)
library(ggplot2)

# Inicializacion.

repository.path <- 'C:/Repositorios/SITUM.HACKATHON.CANCELAS'

current.path <- paste(repository.path, 'src', 'main', 'r', sep = .Platform$file.sep)
data.path <- paste(repository.path, 'src', 'data', 'raw', sep = .Platform$file.sep)
output.path <- paste(repository.path, 'src', 'data', 'processed', sep = .Platform$file.sep)
maps.path <- paste(repository.path, 'src', 'data', 'maps', sep = .Platform$file.sep)

setwd(current.path)

source('functions/geometry_functions.R')
source('functions/plot_functions.R')
source('functions/preparation_functions.R')

# Se leen los datos.

images <- list()

images[['planta0']] <- png::readPNG(source = paste(output.path,
                                                   'p0_clean.png',
                                                   sep = .Platform$file.sep))

images[['planta1']] <- png::readPNG(source = paste(data.path,
                                                   'P1 - ee99decc-ebfe-47a2-9a94-65c08d302556.png',
                                                   sep = .Platform$file.sep))

images[['planta2']] <- png::readPNG(source = paste(data.path,
                                                   'P2 - 192fc83a-1d2a-4c18-b7fa-1d331cfc851b.png',
                                                   sep = .Platform$file.sep))

data <- read.table(file = paste(data.path,
                                'cartodata.csv',
                                sep = .Platform$file.sep),
                   header = FALSE,
                   sep = ',',
                   colClasses = c('integer',
                                  'character',
                                  'numeric',
                                  'numeric',
                                  'numeric',
                                  'numeric',
                                  'numeric'))

# Se ejecuta la fase de preparacion de los datos.

data.processed <- data.preparation(data)

# Se exportan los datos.

write.table(x = data.processed,
            file = paste(output.path,
                         'cartodata_processed.csv',
                         sep = .Platform$file.sep),
            quote = FALSE,
            sep = ';',
            dec = '.',
            row.names = FALSE,
            col.names = TRUE,
            na = 'NA')

# Se cargan las zonas.

totalY <- 1684

zones <- list()

zones[['planta0']] <- load.zones(paste(maps.path, 'p0', 'p0_vect_zonas.shp', sep = .Platform$file.sep))
zones[['planta0']] <- flip.coordinates(zones[['planta0']], total.y = totalY)

zones[['planta1']] <- load.zones(paste(maps.path, 'p1', 'p1_vect_zonas.shp', sep = .Platform$file.sep))
zones[['planta1']] <- flip.coordinates(zones[['planta1']], total.y = totalY)

zones[['planta2']] <- load.zones(paste(maps.path, 'p2', 'p2_vect_zonas.shp', sep = .Platform$file.sep))
zones[['planta2']] <- flip.coordinates(zones[['planta2']], total.y = totalY)

# Se representan los datos.

plot.data.ggplot2(data = data.processed,
                  floor = 0,
                  image = images[['planta0']],
                  title = 'Planta baja',
                  points = FALSE,
                  density = TRUE,
                  flux = TRUE,
                  k = 20,
                  r = 50,
                  zones = zones[['planta0']],
                  xlims = c(420, 1800),
                  ylims = c(550, 1380))

ggsave(file = paste(output.path,
                    'p0_out.png',
                    sep = .Platform$file.sep),
       dpi = 600,
       width = 8,
       height = 6,
       units = 'in')

plot.data.ggplot2(data = data.processed,
                  floor = 1,
                  image = images[['planta1']],
                  title = 'Planta 1',
                  points = FALSE,
                  density = TRUE,
                  flux = TRUE,
                  k = 20,
                  r = 50,
                  zones = zones[['planta1']],
                  xlims = c(440, 1640),
                  ylims = c(650, 1270))

ggsave(file = paste(output.path,
                    'p1_out.png',
                    sep = .Platform$file.sep),
       dpi = 600,
       width = 8,
       height = 6,
       units = 'in')

plot.data.ggplot2(data = data.processed,
                  floor = 2,
                  image = images[['planta2']],
                  title = 'Planta 2',
                  points = FALSE,
                  density = TRUE,
                  flux = TRUE,
                  k = 20,
                  r = 50,
                  zones = zones[['planta2']],
                  xlims = c(790, 1630),
                  ylims = c(650, 1320))

ggsave(file = paste(output.path,
                    'p2_out.png',
                    sep = .Platform$file.sep),
       dpi = 600,
       width = 8,
       height = 6,
       units = 'in')